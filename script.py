import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF

print("Opening \"antivirus_dataset.csv\"...\n")

dataframe = pd.read_csv("antivirus_dataset.csv",sep="|")

print("File opened !\n")

#profile = dataframe.profile_report(title='Pandas Profiling Report')
#profile.to_file(output_file="output.html")

col_to_drop = ["Name",
               "ExportNb",
               "ImportsNbOrdinal",
               "md5",
               "Machine",
               "MinorImageVersion",
               "MinorLinkerVersion",
               "Name",
               "SectionsMeanRawsize",
               "SectionsMeanVirtualsize",
               "SizeOfHeapCommit",
               "SizeOfOptionalHeader",
               "SizeOfUninitializedData",
               "Subsystem"
               ]

print("Suppression des colonnes inutiles...\n")

dataframe.drop(col_to_drop, axis=1, inplace=True)

np_table = dataframe.to_numpy()

print("Mélange des lignes...\n")

np.random.shuffle(np_table)

print("Découpage du dataset...\n")

X_train = np_table[:100000,:-1]
Y_train = np_table[:100000,-1:].flatten()

print("1 - KNN")
print("2 - MLP")
print("3 - RandomForest")
print("4 - DecisionTree")
choix = input("Choix de l\'algorithme : ")

print("\nEntrainement de l\'IA...\n")

if (choix==str(1)):
    clf = KNeighborsClassifier(n_neighbors=3)
elif (choix==str(2)):
    clf = MLPClassifier(solver='lbfgs')
elif (choix==str(3)):
    clf = RandomForestClassifier(max_depth=30, n_estimators=60, max_features=3)
elif (choix==str(4)):
    clf=DecisionTreeClassifier(max_depth=5)

clf.fit(X_train,Y_train)

print("Prédiction des valeurs...\n")

Y_true = np_table[-38048:,-1:].flatten()
Y_predict = clf.predict(np_table[-38048:,:-1])

acc_score = accuracy_score(Y_true, Y_predict)
print("Accuracy : "+str(acc_score)+"\n")
print(classification_report(Y_true, Y_predict, labels=[1, 2, 3]))