Nous avons un fichier .csv contenant une analyse antivirus de 138048 lignes. Notre but est de créer un modèle IA permettant de classifier les éléments comme étant légitimes ou malicieux.
Nous utilisons différents réseaux de neuronnes, afin de comparer leur performances

Voici les principales étapes du programme :

# 1) Ouverture d'un fichier .csv à l'aide de pandas
# 2) Génération du rapport html "output.html" à l'aide de pandas-profiling. 

Ce rapport se base sur le fichier .csv précédemment ouvert et comprend les informations suivantes :

    Essentials: type, unique values, missing values
    Quantile statistics like minimum value, Q1, median, Q3, maximum, range, interquartile range
    Descriptive statistics like mean, mode, standard deviation, sum, median absolute deviation, coefficient of variation, kurtosis, skewness
    Most frequent values
    Histogram
    Correlations highlighting of highly correlated variables, Spearman, Pearson and Kendall matrices
    Missing values matrix, count, heatmap and dendrogram of missing values

Note: Ce rapport a été généré préalablement et ajouté au dépôt git

# 3) Suppression des colonnes inutiles
En analysant le rapport généré, nous supprimons manuellement les colonnes inutiles à l'entraînement en se basant sur certains critères comme :
 - Deux colonnes fortement corrélées
 - Des colonnes inutiles à la classification des éléments (nom du programme, hash...)

# 4) Mélange aléatoire des lignes à l'aide d'un random shuffle sur la table

# 5) Découpage du datasheet selon le modèle suivant :
- X_train : Les 100 000 premières lignes privées de la dernière colonne "legitimate" nous permettant d'entraîner notre modèle
- Y_train : Les 100 000 premières lignes de la dernière colonne uniquement

Cela correspond à un découpage de 72% pour l'entraînement, 28% pour la prédiction

# 6) Entraînement de l'IA
Selon le choix effectué par l'utilisateur, on entraîne ensuite l'IA selon un des algorithmes suivants, au choix :
- KNN
- MLP
- RandomForest
- DecisionTree

# 7) Prédiction des valeurs
En se basant sur l'entraînement précédent, on prédit les valeurs de la dernière colonne des 38048 lignes restantes (28%) du fichier de données

# Analyse des résultats
On compare les valeurs obtenues avec les vraies valeurs, pour obtenir un pourcentage de réussite.
On établit ensuite un rapport de classification, affichant des valeurs comme le recall, le f1-score

Le score moyen obtenu des différents algorithmes (97%) est très satisfaisant.